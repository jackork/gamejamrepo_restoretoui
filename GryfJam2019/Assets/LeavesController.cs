﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeavesController : MonoBehaviour
{
    public Sprite[] leavesImages;
    public int hits;
    public SpriteRenderer sr;
    Tower PlayerTower;

    void Start()
    { 
        PlayerTower = Tower.Instance;
        PlayerTower.OnHealthChanged +=  UpdateHealth;
        UpdateHealth(); 
    }

    void OnDestroy()
    { 
        PlayerTower.OnHealthChanged -=  UpdateHealth; 
    }

    void UpdateHealth()
    { 
        hits = Mathf.CeilToInt( (float)PlayerTower.currentHealth/100);
        Debug.Log("PlayerTower.currentHealth: "+PlayerTower.currentHealth);
        Debug.Log("hits "+hits);
        sr.sprite = leavesImages[ Mathf.Max(0, Mathf.Min(hits, 4))];
    }
}