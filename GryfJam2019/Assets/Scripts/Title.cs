﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour
{
    GameObject start;

    // Start is called before the first frame update
    void Start()
    {
        start = GameObject.Find("start");
    }

    // Update is called once per frame
    public void LoadGameScene() {
        SceneManager.LoadScene(1);
    }
}
