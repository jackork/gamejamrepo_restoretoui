﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PodniebnaRoslinka : MonoBehaviour
{
   public  Animator anim;
    void Start()
    { 
        InvokeRepeating("Attack",Random.Range(1,9),Random.Range(1,9));
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.name.Contains("enemyAir") == false)
        return;

        Debug.Log($"Killed {col.gameObject.name}");
        Destroy(col.gameObject);
        Tower.Instance.AddSeeds(11);
        
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.name.Contains("enemyAir") == false)
        return;

        Debug.Log($"Killed {col.gameObject.name}");
        Destroy(col.gameObject);
        Tower.Instance.AddSeeds(11);
        
    }

    public void Attack()
    {
        anim.SetTrigger("Attack");
    }
}
