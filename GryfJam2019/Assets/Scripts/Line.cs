﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour
{
    public Transform ScreenProbe;
    public Transform probe;
    Camera cam;
    public LineRenderer lr;
    public float distanceToCam;
    public GameObject TestMissilePrefab;
    public float force = 222;
    void Start()
    {
        cam = Camera.main; 
    }
 
    void Update()
    { 
        probe.transform.position = cam.ScreenToWorldPoint(new Vector3( Input.mousePosition.x,Input.mousePosition.y,distanceToCam));
        Vector3 distance = transform.position - probe.transform.position;
        Debug.Log("distance L: "+distance.magnitude);
      
        ScreenProbe.position = transform.position + distance;
     
        Debug.DrawLine(transform.position, probe.transform.position, Color.green);
        Debug.DrawLine(transform.position, ScreenProbe.position, Color.red);

        var points = new Vector3[2];
        points[0]= transform.position;
        points[1] = ScreenProbe.position;
        lr.SetPositions(points);
       // if (Input.GetKeyDown("space"))
        {
            GameObject newMissile = Instantiate(TestMissilePrefab,transform.position,Quaternion.identity);
            Rigidbody2D rb = newMissile.GetComponent<Rigidbody2D>();
            rb.AddForce(distance*force);
        }
    }
}