﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrzyziemnaRoslina : MonoBehaviour
{
    public Sprite[] sprites;
    public int hits = 0;
    public SpriteRenderer sr;
    int i = 0;
 
    // to gamejamowa gra więc może być ;)
    void Update()
    { 
        sr.sprite = sprites[ Mathf.Max(0, Mathf.Min(hits, 2))];
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        i++;
        if(i%2 == 0) return;// ;)
        hits++;
        if(hits > 3)
        Destroy(gameObject); 
    }
}
