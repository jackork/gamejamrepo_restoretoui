﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{  
    public Text seedsLabel; 

    Tower PlayerTower;

    void Start()
    { 
        PlayerTower = Tower.Instance;
         
        PlayerTower.OnSeedsCountChanged +=  UpdateSeeds;
        UpdateSeeds(); 
    }

    void OnDestroy()
    { 
        PlayerTower.OnSeedsCountChanged -=  UpdateSeeds; 
    }
 
    void UpdateSeeds()
    {
        seedsLabel.text =$"Ziarna {PlayerTower.currentSeeds}";
    } 
}
