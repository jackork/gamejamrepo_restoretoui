﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using System;

public class Tower : MonoBehaviour {
    public int startingHealth = 500;
    public int currentHealth = 500;
    public int currentSeeds = 50;
    public event Action OnHealthChanged = delegate {};
    public event Action OnSeedsCountChanged = delegate {};
    public Text gameOver;
    bool isDead;
    GameObject pienBlackBack;
    GameObject pienGround;
    SpriteRenderer pien;

     
    public static Tower Instance;

    void Awake()
    {
        Instance = this;
        pien = GameObject.Find("pien").GetComponent<SpriteRenderer>();
        pienBlackBack = GameObject.Find("fadeToBlack");
        pienGround = GameObject.Find("podloga");
        pienBlackBack.SetActive(false);
    }
     
    void Start() {
        currentHealth = startingHealth;
    }

    public void TakeDamage (int amount) {
        currentHealth -= amount;
        OnHealthChanged();
        if(currentHealth <= 0 && !isDead)
            Death();
    }

    public void AddSeeds(int amount)
    {
        currentSeeds += amount;
        OnSeedsCountChanged();
    }

    void Death() {
        isDead = true;
        pien.color = Color.red;
        pienBlackBack.SetActive(true);

        GameObject[] gameObjectArray = GameObject.FindGameObjectsWithTag("enemy");
        foreach(GameObject go in gameObjectArray) {
            go.SetActive (false);
        }
        gameObjectArray = GameObject.FindGameObjectsWithTag("hide");
        foreach(GameObject go in gameObjectArray) {
            go.SetActive (false);
        }
        gameOver.text = "GAME  OVER";
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q)){
            TakeDamage(45);
        }
    }
}
