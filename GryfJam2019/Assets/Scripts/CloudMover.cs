﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMover : MonoBehaviour
{
    public int StartX;
    public int EndX;
    public float Speed;

    // Update is called once per frame
    void Update()
    {
        transform.position += Vector3.right * Speed * Time.deltaTime;
        if(transform.position.x > EndX)
        transform.position = new Vector3(StartX,0,0);
    }
}
