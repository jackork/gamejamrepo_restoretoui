﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class enemyGroundMove : MonoBehaviour {
    public static GameObject towerPoint;
    public float speed;
    public int damage;
    private Rigidbody2D rb2d;
    float speedBegin;
    float moveHorizontal;
    float moveVertical;
    float angleDir;
    float sinAngle;
    bool colTower;
    bool colEnemy;
    bool colSlow;
    int attackTimer;
    Tower playerHealth;
    Animator anim;
    AudioSource audioData;

    void Start() {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        speedBegin = speed;
        moveHorizontal = 0.0f;
        moveVertical = 0.0f;
        sinAngle = 0.0f;
        towerPoint = GameObject.Find("Tower");
        playerHealth = towerPoint.GetComponent<Tower>();

        Vector2 currentPosition = new Vector2(transform.position.x,
                                      transform.position.y);

        Vector2 towerPosition = new Vector2(towerPoint.transform.position.x,
                                    towerPoint.transform.position.y);

        angleDir = -currentPosition.x * towerPosition.y +
                    currentPosition.y * towerPosition.x;
        attackTimer = 160;

        audioData = GetComponent<AudioSource>();
    }

    void FixedUpdate() {
        if (angleDir < 0.0f) {
            moveHorizontal = 1.0f;
        } else {
            moveHorizontal = -1.0f;
            gameObject.transform.localScale = new Vector3(-1, 1, 0);
        }

        if (this.transform.position.y > 0) {
            Vector3 _newPosition = transform.position;
            _newPosition.y += Mathf.Sin(sinAngle) * Time.deltaTime;
            transform.position = _newPosition ;
            sinAngle += 0.1f;
        }

        Vector2 movement = new Vector2 (moveHorizontal, moveVertical);
        rb2d.AddForce (movement * speed);
    }

    void Update() {
        if (colTower) {
            speed = 0;
            if (attackTimer == 0) {
                if (playerHealth.currentHealth > 0)
                    playerHealth.TakeDamage(damage);
                anim.SetTrigger("Attacking");
                attackTimer = 160;
                audioData.Play(0);
            } else {
                attackTimer--;
            }
        } else if (colEnemy) {
            speed = 0;
        } else if (colSlow) {
            speed = speedBegin / 2f;
        } else {
            speed = speedBegin;
        }

        if (speed == 0) {
            anim.SetBool("Idle", true);
        } else {
            anim.SetBool("Idle", false);
        }


    }

    void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.name == "Tower") {
            colTower = true;
        }
    }

    void OnTriggerEnter2D(Collider2D coll) {
        if (coll.gameObject.name.Contains("Przyziemna_Roslina")) {
            colSlow = true;
        } else if (coll.gameObject.name == "stopCollider") {
            colEnemy = true;
        }
    }

    void OnTriggerExit2D(Collider2D coll) {
            colEnemy = false;
            colSlow = false;
    }
}
