﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemySpawner : MonoBehaviour {
    public GameObject enemyGround;
    public GameObject enemyAir;
    public int timer;
    public int enemyCount;
    int timerTemp;

    [Range(0.0f, 1.0f)]
        public float groundPercent;
    int groundFinal;

    [Range(0.0f, 1.0f)]
        public float airPercent;
    int airFinal;

    void Start() {
        timerTemp = 0;
        groundFinal = Mathf.RoundToInt(groundPercent * Mathf.Round(enemyCount));
        airFinal = Mathf.RoundToInt(airPercent * Mathf.Round(enemyCount));
    }


    void Update() {
        if (timerTemp == 0 && groundFinal + airFinal > 0) {
            if (GameObject.FindGameObjectsWithTag("enemy").Length < 10) {
                if (groundFinal > 0) {
                    Instantiate(enemyGround, new Vector3(-10, -3.9f, 0),
                                Quaternion.identity);
                    groundFinal--;
                }
                if (groundFinal > 0) {
                    Instantiate(enemyGround, new Vector3(10, -3.9f, 0),
                                Quaternion.identity);
                    groundFinal--;
                }
                if (airFinal > 0) {
                    Instantiate(enemyAir, new Vector3(-10, 2, 0),
                                Quaternion.identity);
                    airFinal--;
                }
                if (airFinal > 0) {
                    Instantiate(enemyAir, new Vector3(10, 2, 0),
                                Quaternion.identity);
                    airFinal--;
                }
            }
            timerTemp = timer;
        } else {
            timerTemp--;
        }

        if (enemyCount == 0)
            Destroy(gameObject);
    }
}
