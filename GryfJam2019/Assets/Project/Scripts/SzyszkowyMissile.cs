﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ESzyszkaType
{
    Damage,
    Podniebna,
    Krzak
}

public class SzyszkowyMissile : MonoBehaviour
{
    private ESzyszkaType myType;

    [SerializeField]
    GameObject podniebnaPrefab;
    [SerializeField]
    GameObject krzakPrefab;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Floor")
        {
            float distance = this.transform.position.y - collision.transform.position.y;
            int wybor = Random.Range(0,2);
            switch (wybor)
            {
                case 0://ESzyszkaType.Podniebna:
                    Instantiate(podniebnaPrefab,
                        new Vector3(this.transform.position.x, this.transform.position.y - distance, 0), Quaternion.identity);
                    break;
                case 1://ESzyszkaType.Krzak:
                    Instantiate(krzakPrefab,
                        new Vector3(this.transform.position.x, this.transform.position.y - distance, 0), Quaternion.identity);
                    break;
            }
            Destroy(this.gameObject);
        }

        if (collision.gameObject.CompareTag("enemy"))
        {
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
    }

    public void SetMissileType(ESzyszkaType typeToSet)
    {
        myType = typeToSet;
    }
}
