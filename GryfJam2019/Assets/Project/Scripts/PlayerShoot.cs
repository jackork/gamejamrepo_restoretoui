﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    [SerializeField]
    GameObject missile;
    [SerializeField]
    float maxAimRange;
    [SerializeField]
    float maxShotPower;
    [SerializeField]
    float powerToLineModifier;
    [SerializeField]
    float poweringSpeed;

    private LineRenderer shotPowerPreview;
    private Quaternion initialMissileRotation;
    private Vector3 lastMousePos;

    private float initialVelocity = 0;

    private ESzyszkaType selectedSzyszka = ESzyszkaType.Podniebna;

    void Start()
    {
        shotPowerPreview = GetComponentInChildren<LineRenderer>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 newMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            newMousePos.z = 0;
            lastMousePos = newMousePos;
            Vector3 headingToPlayer = this.transform.position - lastMousePos;
            Vector3 dirToPlayer = headingToPlayer.normalized;
            Positioning(newMousePos, dirToPlayer);
            shotPowerPreview.enabled = true;
        }


        if (Input.GetMouseButton(0))
        {
            Vector3 newMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            newMousePos.z = 0;

            if (newMousePos != lastMousePos)
            {
                Vector3 headingToPlayer = this.transform.position - newMousePos;
                Vector3 dirToPlayer = headingToPlayer.normalized;

                Positioning(newMousePos, dirToPlayer);

                lastMousePos = newMousePos;
            }

            if (initialVelocity > maxShotPower)
            {
                initialVelocity = maxShotPower;
                float newPowerLineEnd = maxShotPower / powerToLineModifier;
                shotPowerPreview.SetPosition(1, new Vector3(newPowerLineEnd, 0, 0));
            }
            else if (initialVelocity < maxShotPower)
            {
                initialVelocity += poweringSpeed;
                Debug.Log(initialVelocity);
                float newPowerLineEnd = initialVelocity / powerToLineModifier;
                shotPowerPreview.SetPosition(1, new Vector3(newPowerLineEnd, 0, 0));
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if(Tower.Instance.currentSeeds <1) return;
            GetComponentInChildren<Animator>().SetTrigger("Throw");
            shotPowerPreview.SetPosition(1, new Vector3(0, 0, 0));
            shotPowerPreview.enabled = false;
            GameObject shotMissile = Instantiate(missile, shotPowerPreview.transform.position, initialMissileRotation);
            Tower.Instance.AddSeeds(-1);
            Rigidbody2D missileRigidbody = shotMissile.GetComponent<Rigidbody2D>();
            SzyszkowyMissile szyszka = shotMissile.GetComponent<SzyszkowyMissile>();
            szyszka.SetMissileType(selectedSzyszka);
            missileRigidbody.AddForce(shotMissile.transform.right * initialVelocity, ForceMode2D.Impulse);
            initialVelocity = 0;
        }
    }

    private void Positioning(Vector3 mousePos, Vector3 directionToPlayer)
    {
        if (mousePos.x < this.transform.position.x)
        {
            shotPowerPreview.transform.right = directionToPlayer;
            this.transform.rotation = Quaternion.Euler(0, 0, 0);
            float angleToRotate = Vector3.Angle(directionToPlayer, Vector3.right);

            if (mousePos.y < this.transform.position.y)
            {
                if (angleToRotate > maxAimRange)
                {
                    initialMissileRotation = Quaternion.Euler(0, 0, maxAimRange);
                    shotPowerPreview.transform.rotation = Quaternion.Euler(0, 0, maxAimRange);
                }
                else
                    initialMissileRotation = Quaternion.Euler(0, 0, angleToRotate);
            }
            else if (mousePos.y > this.transform.position.y)
            {
                if (angleToRotate > maxAimRange)
                {
                    initialMissileRotation = Quaternion.Euler(0, 0, 360 - maxAimRange);
                    shotPowerPreview.transform.rotation = Quaternion.Euler(0, 0, 360 - maxAimRange);
                }
                else
                    initialMissileRotation = Quaternion.Euler(0, 0, 360 - angleToRotate);
            }
        }
        else
        {
            this.transform.rotation = Quaternion.Euler(0, 180, 0);
            float angleToRotate = Vector3.Angle(directionToPlayer, -Vector3.right);

            if (mousePos.y < this.transform.position.y)
            {
                if (angleToRotate > maxAimRange)
                {
                    initialMissileRotation = Quaternion.Euler(0, 0, 180 - maxAimRange);
                    shotPowerPreview.transform.localRotation = Quaternion.Euler(0, 0, maxAimRange);
                }
                else
                {
                    initialMissileRotation = Quaternion.Euler(0, 0, 180 - angleToRotate);
                    shotPowerPreview.transform.localRotation = Quaternion.Euler(0, 0, angleToRotate);
                }
            }
            else if (mousePos.y > this.transform.position.y)
            {
                if (angleToRotate > maxAimRange)
                {
                    initialMissileRotation = Quaternion.Euler(0, 0, 180 + maxAimRange);
                    shotPowerPreview.transform.localRotation = Quaternion.Euler(0, 0, 360 - maxAimRange);
                }
                else
                {
                    initialMissileRotation = Quaternion.Euler(0, 0, 180 + angleToRotate);
                    shotPowerPreview.transform.localRotation = Quaternion.Euler(0, 0, 360 - angleToRotate);
                }
            }
        }
    }

    public void SelectSzyszkaToShoot(ESzyszkaType selectedType)
    {
        selectedSzyszka = selectedType;
    }
}
